import React, { FC, useState } from "react";
import { Button, Input, Table } from "antd";
import { columns } from "./TableRegions.config";
import { DataTypes } from "../../types/dataTypes";
import { FilterDropdownProps, SortOrder } from "antd/es/table";
import { QUERY_TYPE } from "../../types/queryTypes";

type TableRegionsProps = {
    data: DataTypes[];
    goDetailPage: (order: number) => void;
    setQueryParams: (key: QUERY_TYPE, value?: string) => void;
    searchParam: string;
    countSortValue: SortOrder;
    currentPageParams: string | null;
    loading: boolean;
};

const TableRegions: FC<TableRegionsProps> = ({
    data,
    goDetailPage,
    currentPageParams = "1",
    countSortValue,
    loading,
    searchParam,
    setQueryParams,
}) => {
    const [searchValue, setSearchValue] = useState(searchParam);
    const handleClickRow = (order: number) => {
        goDetailPage(order);
    };
    const getColumnSearchProps = (dataIndex: string) => ({
        filterDropdown: ({
            selectedKeys,
            confirm,
            clearFilters,
        }: FilterDropdownProps) => (
            <div style={{ padding: 8 }}>
                <Input
                    placeholder={`Search ${dataIndex}`}
                    value={searchValue}
                    onChange={(e) => {
                        const { value } = e.target;
                        console.log(value);
                        setSearchValue(value);
                    }}
                    style={{ marginBottom: 8, display: "block" }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys!, confirm!)}
                    size="small"
                    style={{ width: 90 }}
                >
                    Search
                </Button>
                <Button
                    onClick={() => handleReset(clearFilters!)}
                    size="small"
                    style={{ width: 90 }}
                >
                    Reset
                </Button>
            </div>
        ),
    });
    const handleSearch = (selectedKeys: any[], confirm: () => void) => {
        confirm();
        setQueryParams(QUERY_TYPE.SEARCH, searchValue);
    };

    const handleReset = (clearFilters: () => void) => {
        clearFilters();
        setSearchValue("");
        setQueryParams(QUERY_TYPE.SEARCH);
    };

    const customColumn = columns.map((item) => {
        if (item.dataIndex === "territory") {
            return { ...item, ...getColumnSearchProps("territory") };
        }
        if (item.dataIndex === "libraries") {
            return {
                ...item,
                defaultSortOrder: countSortValue,
                sorter: (a: DataTypes, b: DataTypes) =>
                    a.libraries - b.libraries,
            };
        }
        return item;
    });
    return (
        <Table<DataTypes>
            onChange={(p, f, sorter) => {
                if (sorter.order !== countSortValue) {
                    setQueryParams(QUERY_TYPE.COUNT, sorter.order);
                }
                if (p.current && p.current.toString() !== currentPageParams) {
                    setQueryParams(QUERY_TYPE.PAGE, p.current!.toString());
                }
            }}
            onRow={(record) => {
                return {
                    onClick: () => handleClickRow(record.order),
                };
            }}
            pagination={{
                current: currentPageParams
                    ? parseInt(currentPageParams, 10)
                    : 1,
            }}
            rowKey={"order"}
            dataSource={data}
            columns={customColumn}
            loading={loading}
        />
    );
};

export default TableRegions;
