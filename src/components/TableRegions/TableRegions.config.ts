export const columns = [
    {
        title: "Регион",
        dataIndex: "territory",
        key: "territory",
    },
    {
        title: "Колличество библиотек",
        dataIndex: "libraries",
        key: "libraries",
    },
];
