import React from "react";

import { Layout } from "antd";
import { useData } from "../../helpers/dataContext";
import { TableRegions } from "../../components/TableRegions";
import { useHistory, useLocation } from "react-router-dom";

import qs from "qs";
import { QUERY_TYPE } from "../../types/queryTypes";
import { SortOrder } from "antd/es/table";

const Home = () => {
    const { data, loading } = useData();
    const history = useHistory();
    const location = useLocation();

    const goPage = (order: number) => {
        history.push(`/${order}`);
    };
    const setQueryParams = (key: QUERY_TYPE, value?: string) => {
        const fullParams = {
            ...qs.parse(location.search, { ignoreQueryPrefix: true }),
            [`${key}`]: value,
        };
        if (key !== QUERY_TYPE.PAGE && fullParams[QUERY_TYPE.PAGE] !== "1") {
            fullParams[QUERY_TYPE.PAGE] = "1";
        }
        history.push({ search: qs.stringify(fullParams) });
    };

    const allQueryParams = qs.parse(location.search, {
        ignoreQueryPrefix: true,
    }) as { [x: string]: string };
    return (
        <Layout>
            <TableRegions
                countSortValue={allQueryParams[QUERY_TYPE.COUNT] as SortOrder}
                currentPageParams={allQueryParams["page"]}
                searchParam={allQueryParams["search"] || ""}
                goDetailPage={goPage}
                setQueryParams={setQueryParams}
                data={
                    allQueryParams["search"]
                        ? data.filter((el) =>
                              el.territory
                                  .toLowerCase()
                                  .includes(
                                      allQueryParams["search"].toLowerCase()
                                  )
                          )
                        : data
                }
                loading={loading}
            />
        </Layout>
    );
};

export default Home;
