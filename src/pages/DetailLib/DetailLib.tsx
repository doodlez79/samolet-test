import React, { FC } from "react";

import { Descriptions, Layout, Spin } from "antd";
import { useData } from "../../helpers/dataContext";
import { useParams } from "react-router-dom";

const DetailLib: FC = () => {
    const { data, loading } = useData();
    let { id } = useParams<{ id: string }>();

    const currentDataByOrder = data.find((el) => el.order.toString() === id);
    if (loading || !currentDataByOrder) {
        return <Spin />;
    }
    return (
        <Layout
            style={{
                margin: "10px",
                padding: "10px",
            }}
        >
            <Descriptions
                size={"small"}
                title={`Информация о ${currentDataByOrder.fullname}`}
            >
                <Descriptions.Item label="Округ">
                    {currentDataByOrder.territory}
                </Descriptions.Item>
                <Descriptions.Item label="Адресс">
                    {currentDataByOrder.address}
                </Descriptions.Item>
                <Descriptions.Item label="Колличество библиотек">
                    {currentDataByOrder.libraries}
                </Descriptions.Item>
                <Descriptions.Item label="Период">
                    {currentDataByOrder.period}
                </Descriptions.Item>
            </Descriptions>
        </Layout>
    );
};

export default DetailLib;
