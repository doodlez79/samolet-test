import React from "react";
import "./app.css";
import { DataProvide } from "./helpers/dataContext";
import { Navigation } from "./containers/Navigation";

export const App = () => (
    <DataProvide>
        <Navigation />
    </DataProvide>
);
