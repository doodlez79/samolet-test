import React from "react";
import { Route, Switch } from "react-router-dom";
import { Home } from "../../pages/Home";
import { DetailLib } from "../../pages/DetailLib";

const Navigation = () => {
    return (
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/:id" component={DetailLib} />
        </Switch>
    );
};

export default Navigation;
