import React, {
    FC, useContext, useEffect, useState,
} from 'react';
import {getData} from "./api";
import {DataTypes} from "../types/dataTypes";


interface PropsDataContext  {
    loading: boolean
    data: DataTypes[]
}

export const DataContext: React.Context<PropsDataContext> = React.createContext<PropsDataContext>({
    data: [],
    loading: false
});

export const useData = () => useContext<PropsDataContext>(DataContext);

export const DataProvide:FC = ({children}) => {
    const [ data, setData ] = useState([]);
    const [loading, setLoading] = useState(false)


    useEffect(() => {
        setLoading(true)
        getData().then((res) => {
            setLoading(false)
            setData(res)
        }).catch(() => {
            setLoading(false)
        })
    }, []);


    return (
        <DataContext.Provider value={{
            data,
            loading
        }}
        >
            {children}
        </DataContext.Provider>
    );
};
