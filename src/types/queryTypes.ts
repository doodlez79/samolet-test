export enum QUERY_TYPE {
    PAGE = "page",
    SEARCH = "search",
    COUNT = "count",
}
