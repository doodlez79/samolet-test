export type DataTypes= {
    order: number,
    address: string
    fullname: string
    libraries: number
    territory: string
    subscribers: string
    period: string

}
